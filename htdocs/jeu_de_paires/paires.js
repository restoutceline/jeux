// Le jeu comporte 10 motifs différents qui sont numérotés de 1 à 10.
// Le tableau est initialisé avec les numéros de motifs qui se suivent.

var tabPaires = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9];

//Le codage utilisé pour l'état des cartes est le suivant :
//    0 : face cachée
//    1 : face visible
//    -1 : enlevée 
// Au départ toutes les cartes sont présentées face cachée.

var statutCarte = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

// Tableau contenant les numéros des cartes retournées à un moment donné du jeu.

var imagesRetournees = [];

// Cette variable contient le nombre de paires de cartes qui ont déjà été trouvées.

var nbPairesTrouvees = 0;

// Le tableau imgages contient les objets des éléments imgages de l'interface utilisateur.

var images = document.getElementById("cartes").getElementsByTagName("img");

// On parcourt le tableau des objets des éléments imgages, chacun d'eux reçoit une fonction déclenchée par l'événement onclick.
// La fonction ainsi définie est exécutée à chaque fois que l'utilisateur clique sur l'image son rôle est d'appeller controleJeu avec le numéro de l'image cliquée.

for (var i = 0; i < images.length; i++) {
    images[i].noCarte = i; //Ajout de la propriété noCarte à l'objet imgages
    images[i].onclick = function() {
        controleJeu(this.noCarte);
    }
}

// Appel de la fonction initialiseJeu pour mélanger les cartes.

initialiseJeu();

// La fonction majAffichage met à jour l'affichage de la carte dont on passe le numéro en paramètre.

// L'affichage rendu dépend de l'état actuel de la carte (donné par le tableau statutCarte) :
//    état 0 : carte face cachée, on affichage l'image de dos de carte : Card.jpg,
//    état 1 : carte retournée, on affiche l'image du motif correspondant, on notera que les différentes images des motifs sont dans les fichiers nommés Card1.jpg, Card2.jpg, etc.,
//    état -1 : carte enlevée du jeu, on cache l'élément imgages.

function majAffichage(noCarte) {
    switch (statutCarte[noCarte]) {
        case 0:
            images[noCarte].src = "img/Card.jpg";
            break;
        case 1:
            images[noCarte].src = "img/Card" + tabPaires[noCarte] + ".jpg";
            break;
        case -1:
            images[noCarte].style.visibility = "hidden";
            break;
    }
}

// La fonction initialiseJeu mélange les numéros de tabPaires.
// Pour cela un algorithme de mélange est utilisé :

function initialiseJeu() {
    for (var position = tabPaires.length - 1; position >= 1; position--) {
        var hasard = Math.floor(Math.random() * (position + 1));
        var sauve = tabPaires[position];
        tabPaires[position] = tabPaires[hasard];
        tabPaires[hasard] = sauve;
    }
}

// On affiche la table et on rappelle le timer.

function afficherTable() {
    let tapisTable = document.getElementById("cartes");//
    tapisTable.style.visibility = "visible";

    // demarre.style.visibility = "hidden";
}

// C'est la fonction controleJeu qui contient le coeur du programme : elle est appelée chaque fois que l'utilisateur clique sur une carte en passant en paramètre le numéro de la carte cliquée.
function controleJeu(noCarte) {

    //    Il est impossible d'avoir plus de deux cartes retournées en même temps, ce test évite que cela arrive, par exemple, si un utilisateur clique à toute vitesse sur plusieurs cartes.
    if (imagesRetournees.length < 2) {

        //    Si la carte cliquée est de dos (état 0) :
        //        on fait passer son état à 1,
        //        on ajoute son numéro au tableau des imagesRetournées,
        //        on fait la mise à jour de son affichage. 
        //    On notera que rien n'est fait pour les états 1 et -1 : cliquer sur une carte déjà retournée ne change rien et cliquer sur une zone de carte enlevée non plus.

        if (statutCarte[noCarte] == 0) {
            statutCarte[noCarte] = 1;
            imagesRetournees.push(noCarte);
            majAffichage(noCarte);
        }

        // Si on se retrouve avec deux cartes retournées, il faut déterminer si elles ont le même motif :
        // si oui : les deux cartes prennent le nouvel état -1 (c'est à dire qu'il faut les enlever) et on incrémente la variable qui compte le nombre de paires trouvées (nbPairesTrouvees),
        // si non : les deux cartes prennent le nouvel état 0 (c'est à dire qu'on les remet de dos).

        if (imagesRetournees.length == 2) {
            var nouvelEtat = 0;
            if (tabPaires[imagesRetournees[0]] == tabPaires[imagesRetournees[1]]) {
                nouvelEtat = -1;
                nbPairesTrouvees++;
                // console.log(nbPairesTrouvees);
            }

            statutCarte[imagesRetournees[0]] = nouvelEtat;
            statutCarte[imagesRetournees[1]] = nouvelEtat;

            // Afin que le joueur ait le temps de voir ce qu'il se passe, on différe la mise à jour de l'affichage des cartes de 950 ms.
            // Enfin au cas où toutes les paires ont été trouvées, on appelle la fonction rejouer
            // La méthode setTimeout() permet de définir un « minuteur » (timer) qui exécute une fonction ou un code donné après la fin du délai indiqué.


            setTimeout(function() {
                majAffichage(imagesRetournees[0]);
                majAffichage(imagesRetournees[1]);
                imagesRetournees = [];
                if (nbPairesTrouvees == 9) {
                    clearInterval(timer); //La méthode clearInterval() efface un timer défini avec la méthode setInterval() .
                    rejouer();
                }
                //j'appelle ma fonction compteurTTentative pour afficher sur l'interface utilisateur le nombres total de paires de cartes retournées
                compteurTentative();

            }, 950);
            
        }
    }
}

//Fonction pour envoyer un message à l'utilisateur que la partie est terminée
function rejouer(){
    document.body.innerHTML = "<h1>Bravo, vous avez réussi !</h1>";
	// alert("bravo!");
    setTimeout(1500)
	location.reload(); //recharge la ressource depuis l'URL actuelle.
}


//je créée un bouton pour que l'utilisateur démarre un chronomètre

let button = document.getElementById("play");

button.addEventListener("click", function () {
    // alert("voulez-vous lancer une partie ?");
    // alert("C'est parti !");
    timer();
})

// ***************CHRONOMETRE***************

// var h2 = document.getElementsByTagName('h2')[0];
// var sec = 0;
// var min = 0;
// var t;

// function tick(){
//     sec++;
//     if (sec >= 60) {
//         sec = 0;
//         min++;
//         if (min >= 60) {
//             min = 0;
//         }
//     }
// }
// function add() {
//     tick();
//     timer();
//     h2.textContent = (min > 9 ? min : "0" + min) +":"  + (sec > 9 ? sec : "0" + sec);
// }
function timer() {
    t = setTimeout(add, 1000);
}


/************************Compte à rebour de 3 minutes*********************************** */

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var threeMinutes = 60 * 3,
        display = document.getElementById('time');
    startTimer(threeMinutes, display);
    initialiseJeu();

};


//****************Compteur nombre total de paires retournées :**************************

//je créée un compteur pour afficher le nombre de coups (tentative) total de la partie
let compteur = document.getElementById("compteur");
let tentative = 0;

tentative = 0;
compteur.innerHTML = 0;

function compteurTentative (){
    compteur.innerHTML ++;
    tentative++;
}






